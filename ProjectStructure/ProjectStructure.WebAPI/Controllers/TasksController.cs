﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Tasks;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Tasks;
using ProjectStructure.Common.DTO.Task;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TasksController : ControllerBase
	{
		TasksQueryHandler _queryHandler;
		TasksCommandHandler _commandHandler;

		public TasksController(TasksQueryHandler queryHandler, TasksCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var tasksDto = await _queryHandler.Handle(new GetAllQuery());
			return Ok(tasksDto);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var taskDto = await _queryHandler.Handle(new GetByIdQuery(id));
			return Ok(taskDto);
		}

		[HttpGet("ByNameLength")]
		public async Task<IActionResult> GetTasksByNameLength(int userId, int maxTaskNameLength = 45)
		{
				var tasks = await _queryHandler.Handle(new GetTasksByNameLengthQuery
				{
					PerformerId = userId,
					MaxTaskNameLegth = maxTaskNameLength
				});
				return Ok(tasks);
		}

		[HttpGet("FinishedTaskIdentities")]
		public async Task<IActionResult> GetFinishedTaskIdentities(int userId, int yearFinished = 2020)
		{
				var taskIdentities = await _queryHandler.Handle(new GetFinishedTaskIdentitiesQuery
				{
					PerformerId = userId,
					YearFinished = yearFinished
				});
				return Ok(taskIdentities);
		}

		[HttpGet("NotFinished")]
		public async Task<IActionResult> GetNotFinishedUserTasks(int performerId)
		{
				var tasks = await _queryHandler.Handle(new GetNotFinishedUserTasksQuery { PerformerId = performerId });
				return Ok(tasks);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] TaskCreateDTO taskDto)
		{
			int createdId = await _commandHandler.Handle(new AddTaskCommand { TaskCreateDto = taskDto });
			return Created($"api/tasks/{createdId}", taskDto);
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] TaskUpdateDTO taskUpdateDto)
		{
			await _commandHandler.Handle(new UpdateTaskCommand { TaskUpdateDto = taskUpdateDto });
			return NoContent();
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			await _commandHandler.Handle(new DeleteTaskCommand { TaskId = id });
			return NoContent();
		}
	}
}
