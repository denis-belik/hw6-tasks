﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Users;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Async = System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		UsersQueryHandler _queryHandler;
		UsersCommandHandler _commandHandler;

		public UsersController(UsersQueryHandler queryHandler, UsersCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public async Async.Task<IActionResult> Get()
		{
			var usersDto = await _queryHandler.Handle(new GetAllQuery());
			return Ok(usersDto);
		}

		[HttpGet("sorted/withTasks")]
		public async Async.Task<IActionResult> GetSortedUsersWithTasks()
		{
			var usersDto = await _queryHandler.Handle(new GetSortedUsersWithTasksQuery());
			return Ok(usersDto);
		}

		[HttpGet("{id}")]
		public async Async.Task<IActionResult> Get(int id)
		{
			var userDto = await _queryHandler.Handle(new GetByIdQuery(id));
			return Ok(userDto);
		}

		[HttpGet("{id}/projectTasksInfo")]
		public async Async.Task<IActionResult> GetUserProjectTasksInfo(int id)
		{
			var usersDto = await _queryHandler.Handle(new GetUserProjectTasksInfoQuery() { UserId = id });
			return Ok(usersDto);
		}

		[HttpGet("{id}/ProjectTasksCount")]
		public async Async.Task<IActionResult> GetProjectTasksCount(int id)
		{
			var dictionary = await _queryHandler.Handle(new GetProjectTasksCountQuery { ProjectAuthorId = id });
			var fakeDictionary = await Async.Task.Run(() =>
				dictionary.Select(keyValue => new FakeProjectTasksCountDictionaryDTO
				{
					Project = keyValue.Key,
					TasksCount = keyValue.Value
				})
			);
			return Ok(fakeDictionary);
		}

		[HttpPost]
		public async Async.Task<IActionResult> Post([FromBody] UserCreateDTO userDto)
		{
			int createdId = await _commandHandler.Handle(new AddUserCommand { UserCreateDto = userDto });
			return Created($"api/users/{createdId}", userDto);
		}

		[HttpPut]
		public async Async.Task<IActionResult> Put([FromBody] UserUpdateDTO userUpdateDto)
		{
			await _commandHandler.Handle(new UpdateUserCommand { UserUpdateDto = userUpdateDto });
			return NoContent();
		}

		[HttpDelete("{id}")]
		public async Async.Task<IActionResult> Delete(int id)
		{
			await _commandHandler.Handle(new DeleteUserCommand { UserId = id });
			return NoContent();
		}
	}
}
