﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Teams;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Teams;
using ProjectStructure.Common.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TeamsController : ControllerBase
	{
		TeamsQueryHandler _queryHandler;
		TeamsCommandHandler _commandHandler;

		public TeamsController(TeamsQueryHandler queryHandler, TeamsCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var teamsDto = await _queryHandler.Handle(new GetAllQuery());
			return Ok(teamsDto);
		}

		[HttpGet("MembersByAge")]
		public async Task<IActionResult> TeamMembersByAge(int minAge = 10)
		{
			var teamMembers = await _queryHandler.Handle(new GetTeamMembersByAgeQuery { MinAge = minAge });
			return Ok(teamMembers);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var teamDto = await _queryHandler.Handle(new GetByIdQuery(id));
			return Ok(teamDto);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] TeamCreateDTO teamDto)
		{
			int createdId = await _commandHandler.Handle(new AddTeamCommand { TeamCreateDto = teamDto });
			return Created($"api/tasks/{createdId}", teamDto);
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] TeamUpdateDTO teamUpdateDto)
		{
			await _commandHandler.Handle(new UpdateTeamCommand { TeamUpdateDto = teamUpdateDto });
			return NoContent();
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			await _commandHandler.Handle(new DeleteTeamCommand { TeamId = id });
			return NoContent();
		}
	}
}
