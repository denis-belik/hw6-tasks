using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.DAL.Context;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Queries.Handlers;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.WebAPI.Extensions;

namespace ProjectStructure.WebAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();

			services.AddAutoMapper(cfg =>
			{
				cfg.AddProfile<UserProfile>();
				cfg.AddProfile<ProjectProfile>();
				cfg.AddProfile<TaskProfile>();
				cfg.AddProfile<TeamProfile>();
				cfg.AddProfile<ResultModelsProfile>();
			});

			services.AddScoped<UsersCommandHandler>();
			services.AddScoped<UsersQueryHandler>();

			services.AddScoped<ProjectsCommandHandler>();
			services.AddScoped<ProjectsQueryHandler>();

			services.AddScoped<TasksCommandHandler>();
			services.AddScoped<TasksQueryHandler>();

			services.AddScoped<TeamsCommandHandler>();
			services.AddScoped<TeamsQueryHandler>();

			services.AddDbContext<CompanyDbContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("CompanyDatabaseConnection")));

			services.AddCors();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseCors(builder => builder
				.WithOrigins("http://localhost:4200"));

			app.ConfigureExceptionHandler();

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
