﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ProjectStructure.BLL.Exceptions;
using System;
using System.Net;

namespace ProjectStructure.WebAPI.Extensions
{
	public static class ExceptionMiddlewareExtensions
	{
		public static void ConfigureExceptionHandler(this IApplicationBuilder app)
		{
			app.UseExceptionHandler(appError =>
			{
				appError.Run(async context =>
				{
					var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

					if (contextFeature != null)
					{
						HttpStatusCode statusCode;
						string message = null;

						if (contextFeature.Error is NotFoundException)
						{
							statusCode = HttpStatusCode.NotFound;
							message = contextFeature.Error.Message;
						}
						else if (contextFeature.Error is ArgumentException)
						{
							statusCode = HttpStatusCode.BadRequest;
							message = contextFeature.Error.Message;
						}
						else
						{
							statusCode = HttpStatusCode.InternalServerError;
							message = "Something went wrong. Sorry.";
						}

						context.Response.StatusCode = (int)statusCode;
						context.Response.ContentType = "application/json";
						await context.Response.WriteAsync(JsonConvert.SerializeObject(message));
					}
				});
			});
		}
	}
}
