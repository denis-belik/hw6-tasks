﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.Tests
{
	public class FakeCompanyDbContext : CompanyDbContext
	{
		public FakeCompanyDbContext(DbContextOptions<CompanyDbContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Team>()
				.Property(team => team.Name)
				.HasColumnName("TeamName");
		}
	}
}
