﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Tasks;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
	public class CommandTests
	{
		protected DbContextOptions<CompanyDbContext> ContextOptions { get; }
		private IMapper mapper;

		public CommandTests()
		{
			ContextOptions = new DbContextOptionsBuilder<CompanyDbContext>()
					.UseInMemoryDatabase("TestCompanyDatabase2")
					.Options;

			mapper = new Mapper(new MapperConfiguration(
				cfg =>
				{
					cfg.AddProfile<UserProfile>();
					cfg.AddProfile<ProjectProfile>();
					cfg.AddProfile<TaskProfile>();
					cfg.AddProfile<TeamProfile>();
					cfg.AddProfile<ResultModelsProfile>();
				}
			));
		}

		#region USER CREATION
		[Fact]
		public async System.Threading.Tasks.Task AddUser_WhenUserCreateDTOIsCorrect_ThenCreateNewUser()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var usersCommandHandler = new UsersCommandHandler(context, mapper);

				var userCreateDto = new UserCreateDTO
				{
					FirstName = "Maynard",
					LastName = "Keenan",
					Email = "mkeenan@gmail.com",
					City = "Los Angeles",
					Birthday = new DateTime(1964, 5, 17),
					RegisteredAt = new DateTime(2020, 1, 1)
				};


				await usersCommandHandler.Handle(new AddUserCommand
				{
					UserCreateDto = userCreateDto
				});

				Assert.Contains<User>(context.Users, user => user.FirstName == "Maynard");
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task AddUser_WhenUserCreateDTOHasIncorrectData_ThenThrowArgumentException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var usersCommandHandler = new UsersCommandHandler(context, mapper);

				var userCreateDtoWithEmptyFirstName = new UserCreateDTO
				{
					FirstName = "  ", // incorrect name
					LastName = "Keenan",
					Email = "mkeenan@gmail.com",
					City = "Los Angeles",
					Birthday = new DateTime(1964, 5, 17),
					RegisteredAt = new DateTime(2020, 1, 1)
				};
				await Assert.ThrowsAsync<ArgumentException>(async () => await usersCommandHandler.Handle(new AddUserCommand
				{
					UserCreateDto = userCreateDtoWithEmptyFirstName
				}));

				var userCreateDtoWithIncorrectEmailPattern = new UserCreateDTO
				{
					FirstName = "Maynard",
					LastName = "Keenan Foo", // incorrect lastname
					Email = "incorrect@gmail",
					City = "Los Angeles",
					Birthday = new DateTime(1964, 5, 17),
					RegisteredAt = new DateTime(2020, 1, 1)
				};
				await Assert.ThrowsAsync<ArgumentException>(async () => await usersCommandHandler.Handle(new AddUserCommand
				{
					UserCreateDto = userCreateDtoWithIncorrectEmailPattern
				}));

				Assert.Empty(context.Users);
			}
		}
		#endregion

		#region MARKING TASK AS FINISHED	
		[Fact]
		public async System.Threading.Tasks.Task UpdateTaskMarkTaskFinished_WhenTaskExistsAndTaskUpdateDTOIsCorrect_ThenUpdateTaskStateWithoutChangingOtherPropertis()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var task = new DAL.Models.Task
				{
					Id = 1,
					Name = "Task 1",
					Description = "Task 1 description",
					State = TaskState.Started,
					CreatedAt = new DateTime(2019, 1, 1),
					FinishedAt = new DateTime(2020, 1, 1),
					PerformerId = 1,
					ProjectId = 1
				};

				context.Add(task);
				context.SaveChanges();

				TaskUpdateDTO taskUpdateDTO = new TaskUpdateDTO
				{
					Id = 1,
					State = TaskState.Finished
				};

				TasksCommandHandler tasksCommandHandler = new TasksCommandHandler(context, mapper);
				await tasksCommandHandler.Handle(new UpdateTaskCommand { TaskUpdateDto = taskUpdateDTO });

				Assert.Collection(context.Tasks,
					task =>
					{
						Assert.Equal("Task 1", task.Name);
						Assert.Equal("Task 1 description", task.Description);
						Assert.Equal(TaskState.Finished, task.State);
						Assert.Equal(new DateTime(2019, 1, 1), task.CreatedAt);
						Assert.Equal(new DateTime(2020, 1, 1), task.FinishedAt);
						Assert.Equal(1, task.PerformerId);
						Assert.Equal(1, task.ProjectId);
					}
				);
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task UpdateTaskMarkTaskFinished_WhenTaskNotFound_ThenUpdateTaskStateWithoutChangingOtherPropertis()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var task = new DAL.Models.Task
				{
					Id = 2,
					Name = "Task 1",
					Description = "Task 1 description",
					State = TaskState.Started,
					CreatedAt = new DateTime(2019, 1, 1),
					FinishedAt = new DateTime(2020, 1, 1),
					PerformerId = 1,
					ProjectId = 1
				};

				context.Add(task);
				context.SaveChanges();

				TaskUpdateDTO taskUpdateDTO = new TaskUpdateDTO
				{
					Id = 1,
					State = TaskState.Finished
				};

				TasksCommandHandler tasksCommandHandler = new TasksCommandHandler(context, mapper);

				await Assert.ThrowsAsync<NotFoundException>(
					async () => await tasksCommandHandler.Handle(new UpdateTaskCommand { TaskUpdateDto = taskUpdateDTO }));
			}
		}
		#endregion

		#region USER ADDITION TO TEAM
		[Fact]
		public async System.Threading.Tasks.Task UpdateUser_WhenUserUpdateDTOIsCorrectAndTeamExists_ThenAddUserToTeam()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var user = new User
				{
					Id = 1,
					FirstName = "Maynard",
					LastName = "Keenan",
					Email = "mkeenan@gmail.com",
					City = "Los Angeles",
					Birthday = new DateTime(1964, 5, 17),
					RegisteredAt = new DateTime(2020, 1, 1),
					TeamId = null
				};
				var team = new Team
				{
					Id = 1,
					Name = "Team 1"
				};

				context.AddRange(user, team);
				context.SaveChanges();

				var userUpdateDto = new UserUpdateDTO
				{
					Id = 1,
					TeamId = 1
				};

				UsersCommandHandler usersCommandHandler = new UsersCommandHandler(context, mapper);
				await usersCommandHandler.Handle(new UpdateUserCommand { UserUpdateDto = userUpdateDto });

				Assert.Collection(context.Users,
					user =>
					{
						Assert.Equal(1, user.TeamId);
						Assert.Equal("Maynard", user.FirstName);
						Assert.Equal("Keenan", user.LastName);
						Assert.Equal("mkeenan@gmail.com", user.Email);
						Assert.Equal("Los Angeles", user.City);
						Assert.Equal(new DateTime(1964, 5, 17), user.Birthday);
						Assert.Equal(new DateTime(2020, 1, 1), user.RegisteredAt);
					});
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task UpdateUser_WhenUserUpdateDTOIsCorrectButTeamNotFound_ThenTrowNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var user = new User
				{
					Id = 1,
					FirstName = "Maynard",
					LastName = "Keenan",
					Email = "mkeenan@gmail.com",
					City = "Los Angeles",
					Birthday = new DateTime(1964, 5, 17),
					RegisteredAt = new DateTime(2020, 1, 1),
					TeamId = null
				};

				context.AddRange(user);
				context.SaveChanges();

				var userUpdateDto = new UserUpdateDTO
				{
					Id = 1,
					TeamId = 1
				};

				UsersCommandHandler usersCommandHandler = new UsersCommandHandler(context, mapper);
			
				await Assert.ThrowsAsync<NotFoundException>(
					async () => await usersCommandHandler.Handle(new UpdateUserCommand { UserUpdateDto = userUpdateDto }));
			}
		}
		#endregion


	}
}
