﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.BLL.Commands.Users
{
	public class UpdateUserCommand : UpdateCommand
	{
		public UserUpdateDTO UserUpdateDto { get; set; }
	}
}
