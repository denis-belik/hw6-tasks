﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.BLL.Commands.Projects
{
	public class UpdateProjectCommand : UpdateCommand
	{
		public ProjectUpdateDTO ProjectUpdateDto { get; set; }
	}
}
