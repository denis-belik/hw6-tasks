﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.BLL.Commands.Projects
{
	public class AddProjectCommand : AddCommand
	{
		public ProjectCreateDTO ProjectCreateDto { get; set; }
	}
}
