﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.BLL.Commands.Tasks
{
	public class AddTaskCommand : AddCommand
	{
		public TaskCreateDTO TaskCreateDto { get; set; }
	}
}
