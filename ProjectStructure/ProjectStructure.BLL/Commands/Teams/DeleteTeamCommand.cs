﻿using ProjectStructure.BLL.Abstractions.Commands;

namespace ProjectStructure.BLL.Commands.Teams
{
	public class DeleteTeamCommand : DeleteCommand
	{
		public int TeamId { get; set; }
	}
}
