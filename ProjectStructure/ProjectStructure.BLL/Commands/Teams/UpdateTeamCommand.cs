﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.BLL.Commands.Teams
{
	public class UpdateTeamCommand : UpdateCommand
	{
		public TeamUpdateDTO TeamUpdateDto { get; set; }
	}
}
