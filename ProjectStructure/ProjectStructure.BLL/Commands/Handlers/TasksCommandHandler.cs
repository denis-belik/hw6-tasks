﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Tasks;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Async = System.Threading.Tasks;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class TasksCommandHandler : CommandHandler
	{
		public TasksCommandHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override async Async.Task<int> Handle(AddCommand addCommand)
		{
			AddTaskCommand command = addCommand as AddTaskCommand;
			TaskCreateDTO taskCreate = command.TaskCreateDto;

			if (!await _context.Users.AnyAsync(user => user.Id == taskCreate.PerformerId))
			{
				throw new NotFoundException(nameof(User), taskCreate.PerformerId);
			}

			if (!await _context.Projects.AnyAsync(project => project.Id == taskCreate.ProjectId))
			{
				throw new NotFoundException(nameof(Team), taskCreate.ProjectId);
			}

			Task newTask = _mapper.Map<Task>(taskCreate);

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Task>(newTask);

			await _context.Tasks.AddAsync(newTask);
			await _context.SaveChangesAsync();

			return newTask.Id;
		}

		public override async Async.Task Handle(DeleteCommand deleteCommand)
		{
			DeleteTaskCommand command = deleteCommand as DeleteTaskCommand;

			Task task = await _context.Tasks.FirstOrDefaultAsync(task => task.Id == command.TaskId);

			if (task == null)
			{
				throw new NotFoundException(nameof(Task), command.TaskId);
			}

			_context.Tasks.Remove(task);
			await _context.SaveChangesAsync();
		}

		public override async Async.Task Handle(UpdateCommand updateCommand)
		{
			UpdateTaskCommand command = updateCommand as UpdateTaskCommand;
			TaskUpdateDTO taskUpdate = command.TaskUpdateDto;

			Task taskEntity = await _context.Tasks.FirstOrDefaultAsync(task => task.Id == taskUpdate.Id);

			if (taskEntity == null)
			{
				throw new NotFoundException(nameof(Task), taskUpdate.Id);
			}

			taskEntity.Name = string.IsNullOrEmpty(taskUpdate.Name?.Trim())
				? taskEntity.Name
				: taskUpdate.Name;

			taskEntity.Description = string.IsNullOrEmpty(taskUpdate.Description?.Trim())
				? taskEntity.Description
				: taskUpdate.Description;

			taskEntity.FinishedAt = taskUpdate.FinishedAt ?? taskEntity.FinishedAt;

			if(taskUpdate.State != null)
			{
				taskEntity.State = (TaskState)taskUpdate.State;
			}
			
			if(taskUpdate.PerformerId != null)
			{
				if(!await _context.Users.AnyAsync(user => user.Id == taskUpdate.PerformerId))
				{
					throw new NotFoundException(nameof(User), (int)taskUpdate.PerformerId);
				}
				taskEntity.PerformerId = (int)taskUpdate.PerformerId;
			}

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Task>(taskEntity);

			_context.Update(taskEntity);
			await _context.SaveChangesAsync();
		}
	}
}
