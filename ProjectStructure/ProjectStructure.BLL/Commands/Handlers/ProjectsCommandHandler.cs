﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Projects;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Linq;
using Async = System.Threading.Tasks;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class ProjectsCommandHandler : CommandHandler
	{
		public ProjectsCommandHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override async Async.Task<int> Handle(AddCommand addCommand)
		{
			AddProjectCommand command = addCommand as AddProjectCommand;
			ProjectCreateDTO projectCreate = command.ProjectCreateDto;

			if(!await _context.Users.AnyAsync(user => user.Id == projectCreate.AuthorId))
			{
				throw new NotFoundException(nameof(User), projectCreate.AuthorId);
			}

			if(!await _context.Teams.AnyAsync(task => task.Id == projectCreate.TeamId))
			{
				throw new NotFoundException(nameof(Team), projectCreate.TeamId);
			}

			Project newProject = _mapper.Map<Project>(projectCreate);

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Project>(newProject);

			await _context.Projects.AddAsync(newProject);
			await _context.SaveChangesAsync();

			return newProject.Id;
		}

		public override async Async.Task Handle(DeleteCommand deleteCommand)
		{
			DeleteProjectCommand command = deleteCommand as DeleteProjectCommand;

			Project project = await _context.Projects.FirstOrDefaultAsync(project => project.Id == command.ProjectId);

			if (project == null)
			{
				throw new NotFoundException(nameof(Project), command.ProjectId);
			}

			_context.Projects.Remove(project);
			await _context.SaveChangesAsync();
		}

		public override async Async.Task Handle(UpdateCommand updateCommand)
		{
			UpdateProjectCommand command = updateCommand as UpdateProjectCommand;
			ProjectUpdateDTO projectUpdate = command.ProjectUpdateDto;

			Project projectEntity = await _context.Projects.FirstOrDefaultAsync(project => project.Id == projectUpdate.Id);

			if (projectEntity == null)
			{
				throw new NotFoundException(nameof(Project), projectUpdate.Id);
			}

			projectEntity.Name = string.IsNullOrEmpty(projectUpdate.Name?.Trim()) 
							   ? projectEntity.Name 
							   : projectUpdate.Name;

			projectEntity.Description = string.IsNullOrEmpty(projectUpdate.Description?.Trim())
									  ? projectEntity.Description
									  : projectUpdate.Description;

			if(projectUpdate.Deadline > DateTime.Now)
			{
				projectEntity.Deadline = projectUpdate.Deadline;
			}

			if(projectUpdate.TeamId != null)
			{
				if (!await _context.Teams.AnyAsync(team => team.Id == projectUpdate.TeamId))
				{
					throw new NotFoundException(nameof(Team), (int)projectUpdate.TeamId);
				}
				projectEntity.TeamId = (int)projectUpdate.TeamId;
			}

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Project>(projectEntity);

			_context.Projects.Update(projectEntity);
			await _context.SaveChangesAsync();
		}
	}
}
