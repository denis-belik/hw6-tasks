﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Teams;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System.Linq;
using Async = System.Threading.Tasks;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class TeamsCommandHandler : CommandHandler
	{
		public TeamsCommandHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override async Async.Task<int> Handle(AddCommand addCommand)
		{
			AddTeamCommand command = addCommand as AddTeamCommand;
			TeamCreateDTO teamCreate = command.TeamCreateDto;

			Team newTeam = _mapper.Map<Team>(teamCreate);

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Team>(newTeam);

			await _context.Teams.AddAsync(newTeam);
			await _context.SaveChangesAsync();

			return newTeam.Id;
		}

		public override async Async.Task Handle(DeleteCommand deleteCommand)
		{
			DeleteTeamCommand command = deleteCommand as DeleteTeamCommand;

			Team team = await _context.Teams.FirstOrDefaultAsync(team => team.Id == command.TeamId);

			if (team == null)
			{
				throw new NotFoundException(nameof(Team), command.TeamId);
			}

			_context.Teams.Remove(team);
			await _context.SaveChangesAsync();
		}

		public override async Async.Task Handle(UpdateCommand updateCommand)
		{
			UpdateTeamCommand command = updateCommand as UpdateTeamCommand;
			TeamUpdateDTO teamUpdate = command.TeamUpdateDto;

			Team teamEntity = await _context.Teams.FirstOrDefaultAsync(team => team.Id == teamUpdate.Id);

			if (teamEntity == null)
			{
				throw new NotFoundException(nameof(Team), teamUpdate.Id);
			}

			teamEntity.Name = string.IsNullOrEmpty(teamUpdate.Name?.Trim())
					  ? teamEntity.Name
					  : teamUpdate.Name;

			// validate properties due to data annotations
			await ValidationHelper.ValidateProperties<Team>(teamEntity);

			_context.Teams.Update(teamEntity);
			await _context.SaveChangesAsync();
		}
	}
}
