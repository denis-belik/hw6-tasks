﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Tasks;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Async = System.Threading.Tasks;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class TasksQueryHandler : QueryHandler<TaskDTO>
	{
		public TasksQueryHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override async Async.Task<TaskDTO> Handle(GetByIdQuery query)
		{
			var task = await _context.Tasks.FirstOrDefaultAsync(task => task.Id == query.EntityId);

			if (task == null)
			{
				throw new NotFoundException(nameof(DAL.Models.Task), query.EntityId);
			}

			return _mapper.Map<TaskDTO>(task);
		}

		public override async Async.Task<IEnumerable<TaskDTO>> Handle(GetAllQuery query)
		{
			var tasks = await _context.Tasks.ToListAsync();
			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}

		// 2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
		public async Async.Task<IEnumerable<TaskDTO>> Handle(GetTasksByNameLengthQuery query)
		{
			if (!await _context.Users.AnyAsync(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			var tasks = await _context.Tasks
				.Include(task => task.Performer)
				.Where(task => task.PerformerId == query.PerformerId && task.Name.Length < query.MaxTaskNameLegth)
				.ToListAsync();

			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}

		// 3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).
		public async Async.Task<IEnumerable<TaskIdentitiesDTO>> Handle(GetFinishedTaskIdentitiesQuery query)
		{
			if (!await _context.Users.AnyAsync(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			var tasks = await _context.Tasks.Where(task =>
					task.PerformerId == query.PerformerId &&
					task.State == TaskState.Finished &&
					task.FinishedAt.Year == query.YearFinished)
				.ToListAsync();

			var taskIdentities = tasks.Select(task => new TaskIdentitiesDTO { Id = task.Id, Name = task.Name });

			return taskIdentities;
		}

		//  Вибірка всіх невиконаних завдань для користувача (з усіх проектів)
		public async Async.Task<IEnumerable<TaskDTO>> Handle(GetNotFinishedUserTasksQuery query)
		{
			if(!await _context.Users.AnyAsync(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			var tasks = await _context.Tasks
				.Where(task => task.PerformerId == query.PerformerId)
				.Where(task => task.State != TaskState.Finished)
				.ToListAsync();

			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}
	}
}
