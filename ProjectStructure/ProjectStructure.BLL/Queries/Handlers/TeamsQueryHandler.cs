﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Teams;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class TeamsQueryHandler : QueryHandler<TeamDTO>
	{
		public TeamsQueryHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override async Task<TeamDTO> Handle(GetByIdQuery query)
		{
			var team = await _context.Teams.FirstOrDefaultAsync(team => team.Id == query.EntityId);

			if (team == null)
			{
				throw new NotFoundException(nameof(Team), query.EntityId);
			}

			return _mapper.Map<TeamDTO>(team);
		}

		public override async Task<IEnumerable<TeamDTO>> Handle(GetAllQuery query)
		{
			var teams = await _context.Teams.ToListAsync();
			return _mapper.Map<ICollection<TeamDTO>>(teams);
		}

		// 4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
		// відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
		public async Task<IEnumerable<TeamMembersDTO>> Handle(GetTeamMembersByAgeQuery query)
		{
			var sw = Stopwatch.StartNew();

			var users =
				await _context.Users
				.Where(user => DateTime.Now.Year - user.Birthday.Year > query.MinAge)
				.OrderByDescending(user => user.RegisteredAt)
				.Join(
					_context.Teams,
					user => user.TeamId,
					team => team.Id,
					(user, team) => new User(user, team))
				.ToListAsync();

			var teamMembersDtos = 
				users.GroupBy(
					user => user.Team,
					(team, users) => new TeamMembersDTO
					{
						Id = team.Id,
						Name = team.Name,
						Members = _mapper.Map<ICollection<UserDTO>>(users)
					});

			long time = sw.ElapsedMilliseconds;
			return teamMembersDtos;
		}
	}
}
