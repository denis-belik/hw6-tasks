﻿

namespace ProjectStructure.BLL.Queries.Tasks
{
	public class GetFinishedTaskIdentitiesQuery
	{
		public int PerformerId { get; set; }
		public int YearFinished { get; set; } = 2020;
	}
}
