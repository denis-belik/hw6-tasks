﻿using System;

namespace ProjectStructure.BLL.Exceptions
{
    class AlreadyExistsException : Exception
    {
        public AlreadyExistsException(string entity, string entityName)
            : base($"{entity} {entityName} already exists.")
        {
        }

        public AlreadyExistsException(string entity, int entityId)
           : base($"{entity} with id {entityId} already exists.")
        {
        }
    }
}
