﻿using ProjectStructure.DAL.Models;
using System;

namespace ProjectStructure.Common.DTO.Task
{
	public class TaskUpdateDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime? FinishedAt { get; set; }
		public TaskState? State { get; set; }
		public int? PerformerId { get; set; }
	}
}
