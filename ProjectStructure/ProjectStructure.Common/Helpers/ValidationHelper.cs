﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Common.Helpers
{
	public static class ValidationHelper
	{
		public static async Task ValidateProperties<T>(T entity)
		{
			await Task.Run(() => 
			{
				var validationResults = new List<ValidationResult>();
				bool isValid = Validator.TryValidateObject(
					entity,
					new ValidationContext(entity),
					validationResults,
					true);

				if (!isValid)
				{
					string errorMessage = "";
					foreach (var result in validationResults)
					{
						errorMessage += result.ErrorMessage + "\n";
					}
					throw new ArgumentException(errorMessage);
				}
			});	
		}
	}
}
