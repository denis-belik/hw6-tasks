﻿using ProjectStructure.ClientConsoleApp.Services;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using System;
using System.Threading.Tasks;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.ClientConsoleApp.Constants;
using System.Timers;
using System.Diagnostics;
using System.Linq;

namespace ProjectStructure.ClientConsoleApp
{
	class Program
	{
		static HttpService httpService = new HttpService("https://localhost:44365/api/");

		static async Task Main()
		{
			try
			{
				while(true)
				{
					Console.WriteLine("Main menu:");
					Console.WriteLine($"Choose action: {UserActions.Read} | {UserActions.Create} | {UserActions.Update} | {UserActions.Delete} ");
					string action = Console.ReadLine().ToLower().Trim();
					switch (action)
					{
						case UserActions.Read:
							await ReadEntry();
							break;
						case UserActions.Create:
							await CreateEntry();
							break;
						case UserActions.Update:
							await UpdateEntry();
							break;
						case UserActions.Delete:
							await DeleteEntry();
							break;
						default:
							Console.WriteLine("Incorrect action.");
							break;
					}
				}			
			}
			catch(Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex.Message);
				Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black
										? ConsoleColor.White
										: ConsoleColor.Black;
				await Main();
			}		
		}

		#region READ

		static async Task ReadEntry()
		{
			Console.Clear();
			Console.WriteLine("Read menu:");
			Console.WriteLine($"Type \"{ReadType.Tasks}\" to read tasks from homework 1.");
			Console.WriteLine($"Type \"{ReadType.Basic}\" to read basic data.");
			string type = Console.ReadLine().ToLower().Trim();
			switch (type)
			{
				case ReadType.Tasks:
					await TasksReadEntry();
					break;

				case ReadType.Basic:
					await BasicReadEntry();
					break;

				default:
					Console.WriteLine("Incorrect read type.");
					break;
			}
		}

		static async Task TasksReadEntry()
		{
			Console.Clear();
			Console.WriteLine("Tasks read menu:");
			Console.WriteLine("Start: [taskNumber] [entityId]");
			string input = Console.ReadLine().Trim();
			if (!int.TryParse(input[0..1], out int taskNumber))
			{
				throw new ArgumentException("Incorrect taskNumber");
			}
			if (!int.TryParse(input[2..input.Length], out int entityId))
			{
				throw new ArgumentException("Incorrect entityId number");
			}
			Console.Clear();
			Console.WriteLine($"Task {taskNumber}, EntityId {entityId}");
			switch (taskNumber)
			{
				case 1:
					var projectTaskCount = await httpService
						.GetCollection<FakeProjectTasksCountDictionaryDTO>(
							$"users/{entityId}/ProjectTasksCount");
					await ConsoleService.PrintTask1(projectTaskCount);
					break;

				case 2:
					var tasksByNameLength = await httpService
						.GetCollection<TaskDTO>($"tasks/ByNameLength?userId={entityId}");
					await ConsoleService.PrintTask2(tasksByNameLength);
					break;

				case 3:
					var tasksFinishedIn2020 = await httpService
						.GetCollection<TaskIdentitiesDTO>($"tasks/FinishedTaskIdentities?userId={entityId}");
					await ConsoleService.PrintTask3(tasksFinishedIn2020);
					break;

				case 4:
					var teamsWithMembersOlderThan10Years = await httpService
						.GetCollection<TeamMembersDTO>("teams/MembersByAge");
					await ConsoleService.PrintTask4(teamsWithMembersOlderThan10Years);
					break;

				case 5:
					var orderedUsers = await httpService
						.GetCollection<UserWithTasksDTO>("users/sorted/withTasks");
					await ConsoleService.PrintTask5(orderedUsers);
					break;

				case 6:
					var lastProject_tasks = await httpService
						.GetEntity<UserProjectTasksInfoDTO>($"users/{entityId}/projectTasksInfo");
					await ConsoleService.PrintTask6(lastProject_tasks);
					break;

				case 7:
					var project_tasks_membersCount = await httpService
						.GetCollection<ProjectTasksTeamInfoDTO>("projects/TasksTeamInfos");
					await ConsoleService.PrintTask7(project_tasks_membersCount);
					break;
				default:
					Console.WriteLine("Incorrect taskNumber.");
					break;
			}
		}

		static async Task BasicReadEntry()
		{
			Console.Clear();
			Console.WriteLine("Basic read menu:");
			Console.WriteLine($"Choose collection type: {CollectionTypes.User}s | {CollectionTypes.Project}s | {CollectionTypes.Task}s | {CollectionTypes.Team}s");
			Console.WriteLine("To get exact entity, specify its Id after colection type: [collection type] [entity id]");
			Console.WriteLine("Example: users 3 - gets user with id 3.");
			Console.WriteLine("Example: users - gets all users.");

			string entity = Console.ReadLine().ToLower().Trim();
			if(entity.Contains(" ")) // means it also contains entity Id
			{
				int entityIdIndex = entity.IndexOf(" ") + 1;
				if (!int.TryParse(entity[entityIdIndex..entity.Length], out int entityId))
				{
					throw new ArgumentException("Incorrect entityId");
				}
				entity = entity[0..(entityIdIndex - 1)];

				switch (entity)
				{
					case CollectionTypes.User + "s":
						var user = await httpService.GetEntity<UserDTO>($"users/{entityId}");
						await ConsoleService.PrintEntity(user);
						break;
					case CollectionTypes.Project + "s":
						var project = await httpService.GetEntity<ProjectDTO>($"projects/{entityId}");
						await ConsoleService.PrintEntity(project, "Id", "Name", "CreatedAt", "Deadline", "AuthorId", "TeamId");
						break;
					case CollectionTypes.Task + "s":
						var task = await httpService.GetEntity<TaskDTO>($"tasks/{entityId}");
						await ConsoleService.PrintEntity(task, "Id", "Name", "CreatedAt", "FinishedAt", "State", "ProjectId", "PerformerId");
						break;
					case CollectionTypes.Team + "s":
						var team = await httpService.GetEntity<TeamDTO>($"teams/{entityId}");
						await ConsoleService.PrintEntity(team);
						break;
				}
			}
			else
			{
				switch (entity)
				{
					case CollectionTypes.User + "s":
						var users = await httpService.GetCollection<UserDTO>("users");
						await ConsoleService.PrintCollection(users);
						break;
					case CollectionTypes.Project + "s":
						var projects = await httpService.GetCollection<ProjectDTO>("projects");
						await ConsoleService.PrintCollection(projects, "Id", "Name", "CreatedAt", "Deadline", "AuthorId", "TeamId");
						break;
					case CollectionTypes.Task + "s":
						var tasks = await httpService.GetCollection<TaskDTO>("tasks");
						await ConsoleService.PrintCollection(tasks, "Id", "Name", "CreatedAt", "FinishedAt", "State", "ProjectId", "PerformerId");
						break;
					case CollectionTypes.Team + "s":
						var teams = await httpService.GetCollection<TeamDTO>("teams");
						await ConsoleService.PrintCollection(teams);
						break;
				}
			}		
		}

		#endregion


		#region CREATE

		static async Task CreateEntry()
		{
			Console.Clear();
			Console.WriteLine("Create menu:");
			Console.WriteLine($"Choose what entity to create:  {CollectionTypes.User} | {CollectionTypes.Project} | {CollectionTypes.Task} | {CollectionTypes.Team}");
			
			string entity = Console.ReadLine().ToLower().Trim();
			switch (entity)
			{
				case CollectionTypes.User:
					await UserCreateEntry();
					break;

				case CollectionTypes.Project:
					await ProjectCreateEntry();
					break;

				case CollectionTypes.Task:
					await TaskCreateEntry();
					break;
				case CollectionTypes.Team:
					await TeamCreateEntry();
					break;
			}
		}

		static async Task UserCreateEntry()
		{
			Console.Clear();
			Console.WriteLine("User create menu:");

			UserCreateDTO userCreate = await PropertyService.FillProperties<UserCreateDTO>();
			await httpService.PostEntity("users", userCreate);
			Console.WriteLine("User has been created.");
		}
		static async Task ProjectCreateEntry()
		{
			Console.Clear();
			Console.WriteLine("Project create menu:");

			ProjectCreateDTO projectCreate = await PropertyService.FillProperties<ProjectCreateDTO>();
			await httpService.PostEntity("projects", projectCreate);
			Console.WriteLine("Project has been created.");
		}
		static async Task TaskCreateEntry()
		{
			Console.Clear();
			Console.WriteLine("Task create menu:");

			TaskCreateDTO taskCreate = await PropertyService.FillProperties<TaskCreateDTO>();
			await httpService.PostEntity("tasks", taskCreate);
			Console.WriteLine("Task has been created.");
		}
		static async Task TeamCreateEntry()
		{
			Console.Clear();
			Console.WriteLine("Team create menu:");

			TeamCreateDTO teamCreate = await PropertyService.FillProperties<TeamCreateDTO>();
			await httpService.PostEntity("teams", teamCreate);
			Console.WriteLine("Team has been created.");
		}

		#endregion


		#region UPDATE

		static async Task UpdateEntry()
		{
			Console.Clear();
			Console.WriteLine("Update menu:");
			Console.WriteLine($"Choose what entity to update:  {CollectionTypes.User} | {CollectionTypes.Project} | {CollectionTypes.Task} | {CollectionTypes.Team}");
			Console.WriteLine($"Or you can mark random task finished (that's great):  finish");

			string entity = Console.ReadLine().ToLower().Trim();
			switch (entity)
			{
				case CollectionTypes.User:
					await UserUpdateEntry();
					break;

				case CollectionTypes.Project:
					await ProjectUpdateEntry();
					break;

				case CollectionTypes.Task:
					await TaskUpdateEntry();
					break;

				case CollectionTypes.Team:
					await TeamUpdateEntry();
					break;

				case "finish":
					_ = MarkRandomTaskFinishedWithDelay(1000)
						.ContinueWith(task =>
						{
							Console.ForegroundColor = ConsoleColor.Green;
							Console.WriteLine($"Task marked as finished - {task.Result}");
							Console.ForegroundColor = Console.BackgroundColor == ConsoleColor.Black
													? ConsoleColor.White
													: ConsoleColor.Black;
						});
					break;
			}
		}

		static Task<int> MarkRandomTaskFinishedWithDelay(int delay)
		{
			var tcs = new TaskCompletionSource<int>();
			var timer = new Timer(delay);
			ElapsedEventHandler onElapsed = null;

			onElapsed = async (sender, args) =>
			{
				timer.Elapsed -= onElapsed;
				timer.Stop();

				var tasks = await httpService.GetCollection<TaskDTO>("tasks");

				Random random = new Random();
				int randomId = random.Next(1, tasks.Count);
				var randomTask = tasks.FirstOrDefault(task => task.Id == randomId);

				if (randomTask == null)
				{
					tcs.SetException(new InvalidOperationException($"Task {randomId} wasn't found... Better luck next time."));
				}

				await httpService.PutEntity("tasks", new TaskUpdateDTO
				{
					Id = randomTask.Id,
					State = DAL.Models.TaskState.Finished
				});

				tcs.SetResult(randomId);
			};

			timer.Start();
			timer.Elapsed += onElapsed;

			return tcs.Task;
		}
		static async Task UserUpdateEntry()
		{
			Console.Clear();
			Console.WriteLine("User update menu:");
			Console.WriteLine("To skip some property, just press [enter]");

			UserUpdateDTO userUpdate = await PropertyService.FillProperties<UserUpdateDTO>();
			await httpService.PutEntity("users", userUpdate);
			Console.WriteLine("User has been updated.");
		}
		static async Task ProjectUpdateEntry()
		{
			Console.Clear();
			Console.WriteLine("Project update menu:");
			Console.WriteLine("To skip some property, just press [enter]");

			ProjectUpdateDTO projectUpdate = await PropertyService.FillProperties<ProjectUpdateDTO>();
			await httpService.PutEntity("projects", projectUpdate);
			Console.WriteLine("Project has been updated.");
		}
		static async Task TaskUpdateEntry()
		{
			Console.Clear();
			Console.WriteLine("Task update menu:");
			Console.WriteLine("To skip some property, just press [enter]");

			TaskUpdateDTO taskUpdate = await PropertyService.FillProperties<TaskUpdateDTO>();
			await httpService.PutEntity("tasks", taskUpdate);
			Console.WriteLine("Task has been updated.");
		}
		static async Task TeamUpdateEntry()
		{
			Console.Clear();
			Console.WriteLine("Team update menu:");
			Console.WriteLine("To skip some property, just press [enter]");

			TeamUpdateDTO teamUpdate = await PropertyService.FillProperties<TeamUpdateDTO>();
			await httpService.PutEntity("teams", teamUpdate);
			Console.WriteLine("Team has been updated.");
		}

		#endregion


		#region DELETE

		static async Task DeleteEntry()
		{
			Console.Clear();
			Console.WriteLine("Delete menu:");
			Console.WriteLine($"Choose what entity to delete:  {CollectionTypes.User} | {CollectionTypes.Project} | {CollectionTypes.Task} | {CollectionTypes.Team}");
			Console.WriteLine($"And specify id of entity to delete: [entity type] [entity id]");

			string entity = Console.ReadLine().ToLower().Trim();
			int entityIdIndex = entity.IndexOf(" ") + 1;
			if (!int.TryParse(entity[entityIdIndex..entity.Length], out int entityId))
			{
				throw new ArgumentException("Incorrect entityId");
			}
			entity = entity[0..(entityIdIndex - 1)];
			
			switch (entity)
			{
				case CollectionTypes.User:
					await httpService.DeleteEntity("users", entityId);
					Console.WriteLine($"{entity} {entityId} has been deleted.");
					break;
				case CollectionTypes.Project:
					await httpService.DeleteEntity("projects", entityId);
					Console.WriteLine($"{entity} {entityId} has been deleted.");
					break;
				case CollectionTypes.Task:
					await httpService.DeleteEntity("tasks", entityId);
					Console.WriteLine($"{entity} {entityId} has been deleted.");
					break;
				case CollectionTypes.Team:
					await httpService.DeleteEntity("teams", entityId);
					Console.WriteLine($"{entity} {entityId} has been deleted.");
					break;
				default:
					Console.WriteLine("Incorrect type");
					break;
			}
		}

		#endregion
	}
}
