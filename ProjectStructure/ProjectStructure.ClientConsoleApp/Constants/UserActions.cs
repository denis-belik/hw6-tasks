﻿

namespace ProjectStructure.ClientConsoleApp.Constants
{
	public static class UserActions
	{
		public const string Read = "read";
		public const string Create = "create";
		public const string Update = "update";
		public const string Delete = "delete";
	}
}
