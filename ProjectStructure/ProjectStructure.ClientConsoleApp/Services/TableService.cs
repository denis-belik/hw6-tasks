﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.ClientConsoleApp.Services
{
	public class TableService<T>
	{
		public Dictionary<string, int> PropertyMaxLengths { get; set; }
		public IEnumerable<T> Collection { get; set; }

		public TableService(IEnumerable<T> collection, params string[] propertyNames)
		{
			PropertyMaxLengths = new Dictionary<string, int>();
			Collection = collection;

			List<string> propertyNamesList = new List<string>();

			// if property names aren't given, take all properties
			if(propertyNames == null || propertyNames.Length == 0)
			{
				var properties = collection.FirstOrDefault().GetType().GetProperties();
				foreach (var property in properties)
				{
					propertyNamesList.Add(property.Name);
				}
			}
			else
			{
				propertyNamesList = propertyNames.ToList();
			}

			// fill dictionary propertyName - max length property value from collection
			foreach (string propertyName in propertyNamesList)
			{
				var maxLength = collection
					.Select(entity => 
						entity.GetType().GetProperty(propertyName).GetValue(entity)?
						.ToString()
						.Length)
					.Max();

				if (maxLength == null || propertyName.Length > maxLength)
				{
					maxLength = propertyName.Length;
				}

				PropertyMaxLengths[propertyName] = (int)maxLength;
			}
		}

		public async Task<string> GetTable()
		{
			return await Task.Run(() =>
			{
				string table = "| ";
				// create table header
				foreach (var propertyLength in PropertyMaxLengths)
				{
					int padValue = propertyLength.Value - propertyLength.Key.Length;
					string right = propertyLength.Key.PadRight(padValue + propertyLength.Key.Length);

					table += right + " | ";
				}
				table += "\n";

				// create table data
				foreach (var entity in Collection)
				{
					string row = "| ";
					foreach (var propertyLength in PropertyMaxLengths)
					{
						string propertyValue = GetPropertyValue(entity, propertyLength.Key)?.ToString();
						string right = null;

						if (propertyValue == null)
						{
							right = "".PadRight(propertyLength.Value);
						}
						else
						{
							int padValue = propertyValue == null
									 ? propertyLength.Value
									 : propertyLength.Value - propertyValue.Length;
							right = propertyValue.PadRight(padValue + propertyValue.Length);
						}

						row += right + " | ";
					}
					row += "\n";
					table += row;
				}

				return table;
			});		
		}

		public object GetPropertyValue(T entity, string property)
		{
			return entity.GetType().GetProperty(property).GetValue(entity);
		}
	}
}
