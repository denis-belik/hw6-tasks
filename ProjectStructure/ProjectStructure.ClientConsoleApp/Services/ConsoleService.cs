﻿using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.ClientConsoleApp.Services
{
	public static class ConsoleService
	{
		public static async Task PrintCollection<T>(IEnumerable<T> collection, params string[] propertiesToPrint)
		{
			var table = new TableService<T>(collection, propertiesToPrint);
			Console.WriteLine(await table.GetTable());
		}

		public static async Task PrintEntity<T>(T entity, params string[] propertiesToPrint)
		{
			List<T> oneEntityCollection = new List<T> { entity };
			var table = new TableService<T>(oneEntityCollection, propertiesToPrint);
			Console.WriteLine(await table.GetTable());
		}

		public static async Task PrintTask1(IEnumerable<FakeProjectTasksCountDictionaryDTO> projectTasksCount)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 1:");
				Console.WriteLine("Отримати кiлькiсть таскiв у проектi конкретного користувача (по id) (словник, де key буде проект, а value кiлькiсть таскiв).");
				Console.WriteLine();

				Console.WriteLine("Project id | Tasks count | Author id");
				foreach (var keyValuePair in projectTasksCount)
				{
					Console.WriteLine($"{keyValuePair.Project.Id,10} | {keyValuePair.TasksCount,-11} | {keyValuePair.Project.AuthorId}");
				}

				Console.WriteLine();
			});		
		}

		public static async Task PrintTask2(IEnumerable<TaskDTO> tasksByNameLength)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 2:");
				Console.WriteLine("Отримати список таскiв, призначених для конкретного користувача (по id), де name таска <45 символiв (колекцiя з таскiв).");
				Console.WriteLine();

				Console.WriteLine("Task id | Name length | PerformerId | Name");
				foreach (var task in tasksByNameLength)
				{
					Console.WriteLine($"{task.Id,7} {"|"} {task.Name.Length,-11} | {task.PerformerId,-11} | {task.Name}");
				}

				Console.WriteLine();
			});	
		}

		public static async Task PrintTask3(IEnumerable<TaskIdentitiesDTO> tasksFinishedIn2020)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 3:");
				Console.WriteLine("Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished) в поточному (2020) роцi для конкретного користувача (по id).");
				Console.WriteLine();

				Console.WriteLine("Task id | Task name");
				foreach (var task in tasksFinishedIn2020)
				{
					Console.WriteLine($"{task.Id,7} | {task.Name}");
				}
			});
		}

		public static async Task PrintTask4(IEnumerable<TeamMembersDTO> teamsWithMembersOlderThan10Years)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 4:");
				Console.WriteLine("Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд, учасники яких старшi 10 рокiв, вiдсортованих за датою реєстрацiї користувача за спаданням, а також згрупованих по командах.");
				Console.WriteLine();

				Console.WriteLine($"Team id | Team name {"|",3} Team members list");
				foreach (var team in teamsWithMembersOlderThan10Years)
				{
					Console.WriteLine($"{team.Id,7} | {team.Name,-11} | Members:");

					foreach (var member in team.Members)
					{
						Console.WriteLine($"{"|",9} {"|",13} {member.Id,3}, {member.FirstName,10}, {DateTime.Now.Year - member.Birthday.Year,-2} years old, registered {member.RegisteredAt}");
					}
				}
			});		
		}

		public static async Task PrintTask5(IEnumerable<UserWithTasksDTO> orderedUsers)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 5:");
				Console.WriteLine("Отримати список користувачiв за алфавiтом first_name (по зростанню) з вiдсортованими tasks по довжинi name (за спаданням).");
				Console.WriteLine();

				Console.WriteLine($"UserId | FirstName {"|",2} Tasks");
				foreach (var user in orderedUsers)
				{
					Console.WriteLine($"{user.Id,6} | {user.FirstName,-10} | Tasks:");
					foreach (var task in user.Tasks)
					{
						Console.WriteLine($"{"|",8} {"|",12} {task.Id,3} | {task.Name}");
					}
				}
			});
		}

		public static async Task PrintTask6(UserProjectTasksInfoDTO lastProject_tasks)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 6:");
				Console.WriteLine("Отримати наступну структуру (передати Id користувача в параметри):");

				Console.WriteLine($"User {lastProject_tasks.User.Id}");
				Console.WriteLine($"Останнiй проект користувача(за датою створення) {lastProject_tasks.LatestProject.Id}");
				Console.WriteLine($"Загальна кiлькiсть таскiв пiд останнiм проектом {lastProject_tasks.LatestProjectTasksCount}");
				Console.WriteLine($"Загальна кiлькiсть незавершених або скасованих таскiв для користувача {lastProject_tasks.UsersNotFinishedTasksCount}");
				Console.WriteLine($"Найтривалiший таск користувача за датою {lastProject_tasks.UsersLongestDurationTask.Id}");
			});
		}

		public static async Task PrintTask7(IEnumerable<ProjectTasksTeamInfoDTO> project_tasks_membersCount)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("Task 7:");
				Console.WriteLine("Отримати таку структуру:");
				Console.WriteLine($"Проект");
				Console.WriteLine($"Найдовший таск проекту (за описом)");
				Console.WriteLine($"Найкоротший таск проекту (по iменi)");
				Console.WriteLine($"Загальна кiлькiсть користувачiв в командi проекту, де або опис проекту >20 символiв, або кiлькiсть таскiв <3");

				Console.WriteLine($"Project | TaskByDescription | TaskByName | UsersCount");
				foreach (var project in project_tasks_membersCount)
				{
					Console.WriteLine($"{project.Project.Id,7} | {project.LongestDescriptionTask?.Id,-17} | {project.ShortestNameTask?.Id,-10} | {project.TeamMembersCount}");
				}
			});
		}
	}
}
