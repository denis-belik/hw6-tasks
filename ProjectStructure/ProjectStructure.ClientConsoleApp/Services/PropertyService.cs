﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace ProjectStructure.ClientConsoleApp.Services
{
	public static class PropertyService
	{
		public static async Task<T> FillProperties<T>() where T: new()
		{
			return await Task.Run(() =>
			{
				T entity = new T();
				var properties = entity.GetType().GetProperties();
				foreach (var property in properties)
				{
					var converter = TypeDescriptor.GetConverter(property.PropertyType);

					if (converter is DateTimeConverter)
					{
						Console.Write($"Enter {property.Name} (example: {DateTime.Now.Date}): ");
					}
					else
					{
						Console.Write($"Enter {property.Name}: ");
					}

					string propertyValueString = Console.ReadLine();

					if (string.IsNullOrEmpty(propertyValueString))
					{
						if (property.PropertyType.IsValueType)
						{
							var value = Activator.CreateInstance(property.PropertyType);
							property.SetValue(entity, value);
						}
						else
						{
							property.SetValue(entity, null);
						}
					}
					else
					{
						var value = converter.ConvertFrom(propertyValueString);
						property.SetValue(entity, value);
					}
				}

				return entity;
			});
		}
	}
}
